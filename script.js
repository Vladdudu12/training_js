///VARIABILE

//aratam ca la const trebuie initializat inainte de a putea fi folosit

let x;
x = 5;
console.log(x);
const y = 3; //aici da eroare daca nu initializam

//explicam pe scurt de ce nu se foloseste var
var z; //explicam ca standard-ul este cu let

x = "da"; //string
console.log(x);
console.log(typeof x);
x = true; //bool
console.log(x);
console.log(typeof x);
x = 2.3; //float
console.log(x);
console.log(typeof x);
x = 5; //int
console.log(x);
console.log(typeof x);

y = 8;
console.log(y);

x = x + 1;
x += 1;

let p = 5;

console.log(p++);
console.log(p);
//OPERATORI

z = "5";

let adevarat = true;
let fals = false;

console.log(x > y);
console.log(x < y);

console.log(x >= y);
console.log(x <= y);

console.log(x == y);
console.log(x === z);

console.log(x != y);

console.log(adevarat || fals);
console.log(adevarat && fals);

console.log(x == 5 && y == 3);
console.log(x == 5 && y == 5);

console.log(x == 5 || y == 5);
console.log(x == 3 || y == 3);

console.log(adevarat ? "da" : "nu");
console.log(fals ? "da" : "nu");

//STRUCTURA DECIZIONALA
//TOOD: ADAUGAT ACOLADE
if (x == 5) console.log("x este egal cu 5");
if (x == 3) console.log("x este egal cu 3");

if (adevarat == true) console.log("Am intrat in if");
if (adevarat != true) console.log("Si aici am intrat in if");

if (adevarat) console.log("Chiar am intrat in if");

if (!adevarat) console.log("Oare am intrat in if?");

if (x == 5) if (y == 3) console.log("x este egal cu 5 si y este egal cu 3");

if (x == 5) console.log("x este egal cu 5");
else console.log("x nu este egal cu 5");

if (x == 5) {
    console.log("x este egal cu 5");
} else if (y == 3) {
    console.log("x nu este egal cu 5, dar y este egal cu 3");
} else {
    console.log("x nu este egal cu 5 si nici y nu este egal cu 3");
}

//LOOP-URI
let contor = 0;

//!!!DE MENTIONAT -> DACA UITI SA INCREMENTEZI CONTORUL, AJUNGI LA O BUCLA INFINITA
//CONDITIE contor > 5
while (contor > 5) {
    console.log(contor);
    contor++;
}

contor = 0;
do {
    console.log(contor);
    contor++;
} while (contor < 5);
console.log(contor);

contor = 0;
for (contor; contor < 5; contor++) {
    console.log(contor);
}

//daca vrem sa mentionam si ca putem sa declaram contorul inline

//STRING-URI

//putem sa explicam exemple de ASA NU
// const badString1 = This is a test;
// const badString2 = 'This is a test;
// const badString3 = This is a test';

const string = "The revolution will not be televised.";
console.log(string);

const stringNou = string;
console.log(stringNou);

const sgl = "Single quotes.";
const dbl = "Double quotes";
console.log(sgl);
console.log(dbl);

console.log("Numarul meu este " + x);

const greeting = `Hello,`;
const yourName = "Miqi";
console.log(greeting + yourName);
//aici nu avem spatiu si explicam ce putem sa modificam ca sa fie ok
console.log(greeting + " " + yourName);

console.log("Numele meu are " + yourName.length + " litere");
//aratam metoda mai buna

console.log(`Numarul meu este ${x}`);

console.log(`Hello, ${yourName}`);

const betterGreeting = `Hello, ${yourName}! Ce mai faci?`;
console.log(betterGreeting);

//ARRAYS

let array = [1, 2, 3, 4, 5, 6];
for (index = 0; index < array.length; index++) {
    console.log(array[index]);
}

// For in vs for of
let days = ["monday", "tuesday", "wednesday", "thursday", "friday"];
// afiseaza cheile (indexul)
for (let day in days) {
    console.log(day);
}
// afiseaza valorile
for (let day of days) {
    console.log(day);
}

// While loop
while (index < array.length) {
    console.log(array[index]);
    index++;
}

// For each (nu e bine sa facem await intr-un for each loop)
// for each e content asincron
array.forEach(myFunction);
function myFunction(item, index) {
    console.log(item);
}
array.forEach((item) => console.log(item));

// Map - aplică o funcție fiecărui element si returneaza un nou array (printre singurele metode bune de a face deep copy in JS)
square = (x) => Math.pow(x, 2);
squares = array.map(square);
console.log(array);
console.log(squares);

// Filter - aplică o filtrare și întoarce array-ul modificat
even = (x) => x % 2 === 0;
evens = array.filter(even);
console.log(array);
console.log(evens);

// metode pe arrays
// pop, push, shift, unshift
// splice(start[, deleteCount, elem1, ..., elemN])
// slice([start], [end])
let arr = ["a", "b", "c", "d"];
console.log(arr.slice(1, 3));
// concat, index of

// destructuring
// --- ex 3 ---
let data = [22, 160, "student"];
let [age, height, occupation] = data;
console.log(age);
console.log(height);
console.log(occupation);

function runners() {
    return ["Sandra", "Ola", "Chi"];
}
var [a, b, c] = runners();

var planets = ["Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune", "Pluto"];
var [first, , third, ...others] = planets;
console.log(first);
console.log(third);
console.log(others);

//FUNCTII

console.log("Hello, " + "Dudu");
console.log("Hello, " + "Dudu1");
console.log("Hello, " + "Dudu2");
console.log("Hello, " + "Dudu3");

//ce face un parametru -> o variabila locala a functiei a carei valoare se seteaza la apelul functiei
function afisareNume(nume) {
    console.log("Hello, " + nume);
}

afisareNume("Mihai");
afisareNume("DuduCuRosu");

for (let i = 0; i < nume.length; i++) afisareNume(nume[i]);

//de prezentat ca orice e dupa return nu se ia in calcul
//aratam si ca putem schimba numele functiei si face tot acelasi lucru

function suma(a, b) {
    let rezultat;
    rezultat = a + b;
    return rezultat;
}

//transformam functia sa fie rezolvata doar din return
let rez = suma(10, 30);
console.log(rez);
console.log(suma(10, 30));

//aratam ca nu trebuie aceeasi denumire de variabile ca si parametru declarat in functie
let n = 15;
let m = 25;

console.log("Suma celor doua valori este: " + suma(n, m));

console.log("n este egal cu " + n);
console.log("m este egal cu " + m);

const sumaArrow = (a, b) => {
    return a + b;
};

console.log(sumaArrow(n, m));

//OBIECTE

let obiect = {};

let masina = {
    marca: "CrocoMobil",
    culoare: "Verde",
    anFabricatie: 2010,
    asigurata: true,
    //Adaugam viteza si functiile
    viteza: 10,

    acceleratie: function () {
        viteza = viteza + 10;
    },

    frana: function () {
        viteza = 0;
    },
};

console.log(masina.marca);
console.log(masina.culoare);
console.log(masina.anFabricatie);
console.log(masina.asigurata);

//aratam ca putem sa modificam
masina.asigurata = false;
console.log(masina.asigurata);

//aratam ca putem sa adaugam proprietati din cod
masina.numeProprietar = "Liviu";

//adaugi in obiect viteza si functiile
//spunem ca functiile din cadrul obiectelor se numesc metode
console.log(masina.viteza);

masina.acceleratie();
console.log(masina.viteza);

masina.frana();
console.log(masina.viteza);

//destructuring
let { marca, culoare, anFabricatie } = masina;

console.log(marca);
console.log(culoare);
console.log(anFabricatie);

//alias-uri pe destructuring
let { vitezaDeDeplasare: viteza } = masina;
console.log(viteza); //nu merge
console.log(vitezaDeDeplasare); //merge

//promise-uri

let promise = new Promise((resolve, reject) => {
    const a = 1;
    if (typeof a === "number") {
        resolve("Success!");
    } else {
        reject("Failed!");
    }
});

promise
    .then((message) => {
        console.log(message);
    })
    .catch((message) => {
        console.log(message);
    });

//Exemplu 2

const promiseFunction = new Promise((resolve, reject) => {
    resolve("Function1 resolved");
});

const promiseFunction2 = new Promise((resolve, reject) => {
    resolve("Function2 resolved");
});

Promise.all([promiseFunction, promiseFunction2]).then((messages) => {
    console.log(messages);
    for (const message of messages) {
        console.log(message);
    }
});

// Exemplu 3
const axios = require("axios");
const API_URL = "https://www.fruityvice.com/api/fruit/all";

const getExample = async () => {
    const response = await axios.get(API_URL);

    return response.data;
};

const main = async () => {
    const data = await getExample();
    console.log(data);
};

main();

// Exemplu 4

const getExampleWithValidations = async () => {
    try {
        const response = await axios.get(API_URL);

        //validari
        if (response.status !== 200) {
            //throw new Error("Status not good");
            //throw 400;
            //return Promise.reject("Status not good");
            return Promise.reject(new Error(`Bad status! Status value is ${response.status}`));
        }

        return response.data;
    } catch (err) {
        console.log(err);
    }
};

const mainWithValidations = async () => {
    try {
        const data = await getExampleWithValidations();
        console.log(data);
    } catch (err) {
        console.log(err);
    }
};

mainWithValidations();
